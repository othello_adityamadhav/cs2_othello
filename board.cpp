#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
    Move* move1 = new Move(3, 3);
    Move* move2 = new Move(3, 4);
    Move* move3 = new Move(4, 3);
    Move* move4 = new Move(4, 4);
    pieces[move1] = BLACK;
    pieces[move2] = WHITE;
    pieces[move3] = WHITE;
    pieces[move4] = BLACK;
    
      
}

/*
 * Destructor for the board.
 */
Board::~Board() {
    //std::cerr << "deconstructorBoard" << std::endl;
    //delete move1;
    //std::cerr << "deletemove1" << std::endl;
    //delete move2;
    //delete move3;
    //delete move4;
    //std::cerr <<"deconstructorendBoard" << std::endl;
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}


bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns a vector of all the moves possible for a given side.
 */
vector<Move*> Board::getMoves(Side side)
{
    std::vector<Move*> moves;
    for (int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            Move* move = new Move(i, j);
            if (checkMove(move, side))
            {
                moves.push_back(move);
            }
            //delete (move);
        }
    }
    return moves;
}
            
   

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
int Board::score(Move *m, Side side) {
    
    int cornerBonus = 1000;
    int cornerNeighborBonus = 500;
    int totalScore = 0;
    int subScore = 0;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do { 
	      x += dx;
	      y += dy;

            } while (onBoard(x, y) && get(other, x, y));
	      
	    if((x == 7 || x == 0) && (y == 7 || y == 0))
	      {
		subScore += cornerBonus;
		//fprintf(stderr, "at neighbor\n");
	      }
	    else if((x == 0 || x == 1 || x == 6 || x == 7) && (y == 0 || y == 1 || y == 6 || y == 7))
	      {
		subScore -= cornerNeighborBonus;
		//fprintf(stderr, "at corner neighbor\n");
	      }
	    
            if (onBoard(x, y) && get(side, x, y)) 
	      {
		subScore += count(side);
		totalScore += subScore;
		subScore = 0;
	      }
	    else
	      {
		subScore = 0;
	      }
        }
    }
    return totalScore;
}


/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    Move *set_move = new Move(x, y);
                    pieces[set_move] = side;
                    x += dx;
                    y += dy;
                    delete set_move;
                }
            }
        }
    }
    set(side, X, Y);
    Move *set_move2 = new Move(X, Y);
    pieces[set_move2] = side;
    delete set_move2;
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

std::map<Move*, Side> Board::getPieces(){
    return pieces;
}
