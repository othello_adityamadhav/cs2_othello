#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
  this->board = Board(); //initialize the board;
  this->side = side;
  }

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {

}

/* Evalutes the score of a move given a variety of heuristics
 * including spaces bonus and penalties, and various bonsus and
 * penalties for relative mobility and frontier spaces 
 */

/*
int ExamplePlayer::score(Move *m, Side side)
{
  int score;
  std::map<Move*, Side>::iterator i;
  std::map<Move*, Side> pieces; 
  int x = m->getX();
  int y = m->getX();
  int cornerBonus = 200;
  int cornerNeighborBonus = -30;
  int frontierPenalty = 15;
  int mobilityBonus = 20;
  Side other = (side == BLACK) ? WHITE : BLACK;
  Board tempBoard = this->board;
  tempBoard.doMove(m, side);
  score = tempBoard.count(side) - tempBoard.count(other);
  pieces = tempBoard.getPieces();
  if((x == 7 || x == 0) && (y == 7 || y == 0))
    {
      score += cornerBonus;
    }
  else if((x == 0 || x == 1 || x == 6 || x == 7) && (y == 0 || y == 1 || y == 6 || y == 7))
    {
      score += cornerNeighborBonus;
    }
  for (i = pieces.begin(); i != pieces.end(); i++)
  {
    Move *position = i -> first;
    Side pieces_side = (i -> second);
    Side pieces_other = (pieces_side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;
                int X = position->getX();
                int Y = position->getY();
                Move *move = new Move(X + dx, Y + dy);
                if (tempBoard.isSided(pieces_side, X, Y) && tempBoard.checkMove
                (move, pieces_other))
                {
                    score += frontierPenalty;
                }
                else if (tempBoard.isSided(pieces_other, X, Y) && tempBoard.checkMove
                (move, pieces_side))
                {
                    score -= frontierPenalty;
                }
                if (tempBoard.checkMove(move, pieces_side))
                {
                    score += mobilityBonus;
                }
                else if (tempBoard.checkMove(move, pieces_other))
                {
                    score -= mobilityBonus;
                }
            }
        }
    }
    //std::cerr << score <<std::endl;
}
 */
 
 int ExamplePlayer::score(Board *board, Side side)
{
  int tempScore;
  std::map<Move*, Side>::iterator i;
  std::map<Move*, Side> pieces;
  std::vector<Move*> checkList;
  int cornerBonus = 150;
  int cornerNeighborBonus = -30;
  int frontierPenalty = 0;
  int mobilityBonus = 0;
  int edgeBonus = 50;
  Side other = (side == BLACK) ? WHITE : BLACK;
  Board tempBoard = *board;
  pieces = tempBoard.getPieces();
  tempScore = 0;
  //tempScore = tempBoard.count(side) - tempBoard.count(other);
 
  for (i = pieces.begin(); i != pieces.end(); i++)
    {
      Move *position = i -> first;
      Side pieces_side = (i -> second);
      Side pieces_other = (pieces_side == BLACK) ? WHITE : BLACK;
      for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
      if (dy == 0 && dx == 0) continue;
      int x = position->getX();
      int y = position->getY();
      if((x == 7 || x == 0) && (y == 7 || y == 0) && tempBoard.isSided(side, x, y))
        {
          tempScore += cornerBonus;
          }
      else if((x == 0 || x == 1 || x == 6 || x == 7) && (y == 0 || y == 1 || y == 6 || y == 7) && tempBoard.isSided(side, x, y))
        {
          tempScore += cornerNeighborBonus;
        }
      else if (( x == 0 || x == 7 || y == 0 || y == 0) && tempBoard.isSided(side, x, y))
      {
        tempScore += edgeBonus;
      }
      Move *move = new Move(x + dx, y + dy);
      if((find(checkList.begin(), checkList.end(), move) !=checkList.end()))
      {
        break;
      }
      else
      {
          if (tempBoard.isSided(side, x, y) && tempBoard.checkMove(move, other))
            {
              tempScore += frontierPenalty;
            }
          else if (tempBoard.isSided(other, x, y) && tempBoard.checkMove(move, side))
            {
              continue; //tempScore -= frontierPenalty;
            }
          if (tempBoard.checkMove(move, side))
            {
              tempScore += mobilityBonus;
            }
          else if (tempBoard.checkMove(move, other))
            {
              continue;//tempScore -= mobilityBonus;
            }
        checkList.push_back(move);
       }
      }
        }
      }
    
  //std::cerr << score <<std::endl;
  return tempScore;
}

/* Implements the minimax algorithm. To implement this alogrithm, you look through all of the possible moves of a given 
 *
 *
 *
 * 
 *
 *
 *
 */


ExamplePlayer::MoveScore *ExamplePlayer::minimax(Board *b, int depth, int alpha, int beta, Side side)
{
  MoveScore* finalMS = new MoveScore();
  MoveScore* tempMS = new MoveScore();
  std::vector<Move*> moves = b->getMoves(side);
  std::vector<Move*> :: iterator i;
  Side other = (side == BLACK) ? WHITE : BLACK;
  if (moves.size() == 0 || depth == 0)
    {
      //finalMS->move = NULL;
      finalMS->points = score(b, side);
      /*
      if (side == WHITE)
      {
        finalMS -> points *= -1;
      }
      */
      return finalMS;
    } 
  else
    {
      for (i = moves.begin(); i != moves.end(); i++)
    {
      Board tempBoard = *b;
      tempBoard.doMove(*i, side);
      tempMS = minimax(&tempBoard, depth - 1, -beta, -alpha, other);
      tempMS->points *= -1; 
      int score2 = tempMS->points;
      if(score2 >= beta)
        {
          return tempMS;
        }
      if(score2 >= alpha)
        {
          alpha = score2;
          finalMS->points = alpha;
          finalMS->move = *i;
        }   
    }
    }
  return finalMS;
}       

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
 
        
 
 
/* 
* TODO: Implement how moves your AI should play here. You should first
* process the opponent's opponents move before calculating your own move
*/ 
//Checks every possible move, if valid calculates the score of the move based on simple heuristic
/*
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
  std::cerr<< "VERY BEGINNING" << std::endl;
  int alpha, beta;
  //int tempScore, maxScore;
  //Move checkMove;
  Move *bestMove;
  Side side = this->getSide();
  Side other = (side == BLACK) ? WHITE : BLACK;
  std::vector<Move*> moves;
  //std::vector<Move*> :: iterator i;
  MoveScore *MS;
  alpha = -99999;
  beta = 99999;
  //maxScore = -9999;
  std::cerr << "BEGINNING" << std::endl;
  board.doMove(opponentsMove, other);
    std::cerr << "Do Move" << std::endl;
  moves = board.getMoves(side);
  std::cerr << "gotMoves" << std::endl;
  if (moves.size() == 0)
  {
    return NULL;
  }
  else
  {
    std::cerr << "WHERE IT BEGINS" << std::endl;
    Board tempBoard = this->board;
    MS = minimax(&tempBoard, 3, alpha, beta, side);
    bestMove = MS->move;
  }
  board.doMove(bestMove, side);
  return bestMove;
}
*/
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

  int tempScore, maxScore;
  Move *bestMove;
  maxScore = -9999;
  Side side = this->getSide();
  Side other = (side == BLACK) ? WHITE : BLACK; 
  vector<Move*> moves;
  vector<Move*>::iterator i;
  board.doMove(opponentsMove, other);
  moves = board.getMoves(side);
  int alpha = -99999;
  int beta = 99999;
  MoveScore* MS;
  if (moves.size() == 0)
  {
    return NULL;
  }
  else
  { 

    Board tempBoard = this->board;
    //for(i = moves.begin(); i != moves.end(); i++)
    //{
        //int X = (*i)->getX();
        //int Y = (*i)->getY();
        //std::cerr <<"POSSIBLE MOVES" << std:: endl;
        //std::cerr << X << std::endl;
        //std::cerr << Y << std::endl;
        //Board tempBoard = this ->board;
        //tempBoard.doMove(*i, side);
        //tempScore = score(&tempBoard, side);//minimax(&tempBoard, *i, 3, side);
          //  if (tempScore > maxScore)
            //    {
              //    maxScore = tempScore;
                //  bestMove = (*i);

                //}
        //}

  MS = minimax(&tempBoard, 3, alpha, beta, side);
  bestMove = MS->move;
  board.doMove(bestMove, side);
  return bestMove;
 } 
}

