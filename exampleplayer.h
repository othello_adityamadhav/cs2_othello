#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <algorithm>
using namespace std;

class ExamplePlayer {

 public:
  Side side;
  Board board;
  ExamplePlayer(Side side);
  ~ExamplePlayer();
 
   struct MoveScore
   {
     Move *move;
     int points;
   };
  int score(Move *m, Side side);
  int score(Board *board, Side side);
  MoveScore *minimax(Board *board, int depth, int alpha, int beta, Side side);
  Side getSide() { return side; } 
  Move *doMove(Move *opponentsMove, int msLeft);
 
};

#endif

