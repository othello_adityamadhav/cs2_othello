#ifndef __BOARD_H__
#define __BOARD_H__

#include <vector>
#include <bitset>
#include "common.h"
#include <vector>
#include <map>
#include <iostream>
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
    
    Move* move1;
    Move* move2;
    Move* move3;
    Move* move4;  
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    std::map<Move*, Side> pieces;
    int count(Side side);
    int countBlack();
    int countWhite();
    int score(Move *m, Side side);
    std::vector<Move*> getMoves(Side side);
    bool isOccupied(int x, int y) {return occupied(x, y);};
    bool isSided(Side side, int x, int y) { return get(side, x, y);};
    std::map<Move*, Side> getPieces();
};

#endif
